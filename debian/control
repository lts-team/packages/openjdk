Source: openjdk-22
Section: java
Priority: optional
Maintainer: OpenJDK Team <openjdk-22@packages.debian.org>
Uploaders: Matthias Klose <doko@ubuntu.com>
Build-Depends: debhelper (>= 11),
  m4, lsb-release, zip, unzip,
  sharutils, gawk, cpio, pkg-config, procps, wdiff, fastjar (>= 2:0.96-0ubuntu2),
  time, strip-nondeterminism, debugedit (>= 4.16),
  jtreg7  <!nocheck>, libtestng7-java <!nocheck>, xvfb <!nocheck>, xauth <!nocheck>, xfonts-base <!nocheck>, libgl1-mesa-dri [!x32] <!nocheck>, xfwm4 <!nocheck>, x11-xkb-utils <!nocheck>, dbus-x11 <!nocheck>, libasmtools-java <!nocheck>, googletest <!nocheck>, google-mock <!nocheck>, xvfb  <!nocheck>,
  autoconf, automake, autotools-dev, ant, ant-optional,
  g++-13 <!cross>,
  openjdk-21-jdk-headless:native | openjdk-22-jdk-headless:native,
  libxtst-dev, libxi-dev, libxt-dev, libxaw7-dev, libxrender-dev, libcups2-dev, libasound2-dev, liblcms2-dev, libfreetype6-dev (>= 2.2.1), libxinerama-dev, libkrb5-dev, xsltproc, libpcsclite-dev, libxrandr-dev, libelf-dev, libfontconfig1-dev, libgtk2.0-0 | libgtk-3-0, libharfbuzz-dev,
  libffi-dev, libffi-dev:native,
  zlib1g-dev:native, zlib1g-dev, libattr1-dev, libpng-dev, libjpeg-dev, libgif-dev, 
  libnss3-dev (>= 2:3.17.1),
  openjdk-22-jdk-headless <cross>,
Build-Depends-Indep: graphviz, pandoc,
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://openjdk.java.net/
Vcs-Browser: https://salsa.debian.org/openjdk-team/openjdk
Vcs-Git: https://salsa.debian.org/openjdk-team/openjdk.git

Package: openjdk-22-jdk-headless
Architecture: alpha amd64 arc armel armhf arm64 hppa i386 ia64 mips mipsel mips64 mips64el powerpc ppc64 ppc64el m68k riscv64 sh4 sparc sparc64 s390x x32
Multi-Arch: same
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-22-jre-headless (= ${binary:Version}),
  ${shlibs:Depends}, ${misc:Depends}
Suggests: openjdk-22-demo, openjdk-22-source
Provides: java-sdk-headless (= ${vm:Version}), java2-sdk-headless,
  java5-sdk-headless, java6-sdk-headless,
  java7-sdk-headless, java8-sdk-headless,
  java9-sdk-headless, java10-sdk-headless,
  java11-sdk-headless, java12-sdk-headless,
  java13-sdk-headless, java14-sdk-headless, java15-sdk-headless,
  java16-sdk-headless, java17-sdk-headless,
  java18-sdk-headless, java19-sdk-headless,
  java20-sdk-headless, java21-sdk-headless,
  java22-sdk-headless,
  java-compiler
Description: OpenJDK Development Kit (JDK) (headless)
 OpenJDK is a development environment for building applications,
 applets, and components using the Java programming language.

Package: openjdk-22-jre-headless
Architecture: alpha amd64 arc armel armhf arm64 hppa i386 ia64 mips mipsel mips64 mips64el powerpc ppc64 ppc64el m68k riscv64 sh4 sparc sparc64 s390x x32
Multi-Arch: same
Pre-Depends: ${dpkg:Depends}
Depends: ${jredefault:Depends}, ${cacert:Depends},
  ${jcommon:Depends}, ${dlopenhl:Depends},
  ${mountpoint:Depends},
  ${shlibs:Depends}, ${misc:Depends}
Recommends: ${dlopenhl:Recommends}
Suggests: libnss-mdns,
  fonts-dejavu-extra,
  fonts-ipafont-gothic, fonts-ipafont-mincho, fonts-wqy-microhei | fonts-wqy-zenhei, fonts-indic,
Provides: java-runtime-headless (= ${vm:Version}), java2-runtime-headless,
  java5-runtime-headless, java6-runtime-headless,
  java7-runtime-headless, java8-runtime-headless,
  java9-runtime-headless, java10-runtime-headless,
  java11-runtime-headless, java12-runtime-headless,
  java13-runtime-headless, java14-runtime-headless, java15-runtime-headless,
  java16-runtime-headless, java17-runtime-headless,
  java17-runtime-headless, java19-runtime-headless,
  java20-runtime-headless, java21-runtime-headless,
  java22-runtime-headless,
  ${defaultvm:Provides}, ${jvm:Provides}
Description: OpenJDK Java runtime, using ${vm:Name} (headless)
 Minimal Java runtime - needed for executing non GUI Java programs,
 using ${vm:Name}.

Package: openjdk-22-jdk
Architecture: alpha amd64 arc armel armhf arm64 hppa i386 ia64 mips mipsel mips64 mips64el powerpc ppc64 ppc64el m68k riscv64 sh4 sparc sparc64 s390x x32
Multi-Arch: same
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-22-jre (= ${binary:Version}),
  openjdk-22-jdk-headless (= ${binary:Version}),
  ${shlibs:Depends}, ${misc:Depends}
Recommends: libxt-dev
Suggests: openjdk-22-demo, openjdk-22-source, visualvm
Provides: java-sdk (= ${vm:Version}), java2-sdk, java5-sdk, java6-sdk,
  java7-sdk, java8-sdk, java9-sdk, java10-sdk, java11-sdk,
  java12-sdk, java13-sdk, java14-sdk, java15-sdk, java16-sdk, java17-sdk,
  java18-sdk, java19-sdk, java20-sdk, java21-sdk, java22-sdk,
  java-compiler
Description: OpenJDK Development Kit (JDK)
 OpenJDK is a development environment for building applications,
 applets, and components using the Java programming language.

Package: openjdk-22-jre
Architecture: alpha amd64 arc armel armhf arm64 hppa i386 ia64 mips mipsel mips64 mips64el powerpc ppc64 ppc64el m68k riscv64 sh4 sparc sparc64 s390x x32
Multi-Arch: same
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-22-jre-headless (= ${binary:Version}),
  ${xandsound:Depends}, ${dlopenjre:Depends},
  ${shlibs:Depends}, ${misc:Depends}
Recommends: ${dlopenjre:Recommends}, ${bridge:Recommends}, fonts-dejavu-extra
Provides: java-runtime (= ${vm:Version}), java2-runtime,
  java5-runtime, java6-runtime,
  java7-runtime, java8-runtime,
  java9-runtime, java10-runtime,
  java11-runtime, java12-runtime,
  java13-runtime, java14-runtime, java15-runtime,
  java16-runtime, java17-runtime,
  java18-runtime, java19-runtime,
  java20-runtime, java21-runtime,
  java22-runtime,
Description: OpenJDK Java runtime, using ${vm:Name}
 Full Java runtime environment - needed for executing Java GUI and Webstart
 programs, using ${vm:Name}.

Package: openjdk-22-demo
Architecture: alpha amd64 arc armel armhf arm64 hppa i386 ia64 mips mipsel mips64 mips64el powerpc ppc64 ppc64el m68k riscv64 sh4 sparc sparc64 s390x x32
Priority: optional
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-22-jre (= ${binary:Version}),
  ${shlibs:Depends}, ${misc:Depends}
Description: Java runtime based on OpenJDK (demos and examples)
 OpenJDK Java runtime

Package: openjdk-22-source
Architecture: all
Multi-Arch: foreign
Priority: optional
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-22-jdk (>= ${source:Version}),
  ${misc:Depends}
Description: OpenJDK Development Kit (JDK) source files
 OpenJDK is a development environment for building applications,
 applets, and components using the Java programming language.
 .
 This package contains the Java programming language source files
 (src.zip) for all classes that make up the Java core API.

Package: openjdk-22-doc
Section: doc
Pre-Depends: ${dpkg:Depends}
Architecture: all
Multi-Arch: foreign
Priority: optional
Depends: ${misc:Depends},
  libjs-jquery,
  libjs-jquery-ui,
  libjs-jquery-ui-theme-base,
Suggests: openjdk-22-jdk
Description: OpenJDK Development Kit (JDK) documentation
 OpenJDK is a development environment for building applications,
 applets, and components using the Java programming language.
 .
 This package contains the API documentation.

Package: openjdk-22-dbg
Architecture: alpha amd64 arc armel armhf arm64 hppa i386 ia64 mips mipsel mips64 mips64el powerpc ppc64 ppc64el m68k riscv64 sh4 sparc sparc64 s390x x32
Multi-Arch: same
Priority: optional
Section: debug
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-22-jre-headless (= ${binary:Version}),
   ${misc:Depends}
Recommends: openjdk-22-jre (= ${binary:Version})
Suggests: openjdk-22-jdk (= ${binary:Version})
Conflicts: ${dbg:Conflicts}
Description: Java runtime based on OpenJDK (debugging symbols)
 OpenJDK is a development environment for building applications,
 applets, and components using the Java programming language.
 .
 This package contains the debugging symbols.

Package: openjdk-22-jre-zero
Architecture: amd64 i386 arm64 armhf ppc64 ppc64el riscv64 s390x
Multi-Arch: same
Priority: optional
Pre-Depends: ${dpkg:Depends}
Depends: openjdk-22-jre-headless (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Provides: ${zerovm:Provides}
Description: Alternative JVM for OpenJDK, using Zero
 The package provides an alternative runtime using the Zero VM. Built on
 architectures in addition to the Hotspot VM as a debugging aid for those
 architectures which don't have a Hotspot VM.
 .
 The VM is started with the option `-zero'. See the README.Debian for details.
